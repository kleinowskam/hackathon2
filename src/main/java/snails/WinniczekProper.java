package snails;

public final class WinniczekProper extends GenericSnail {

	public WinniczekProper(){
		shellWeight = 11;
	}
	@Override
	public double crawls() {
		double speed = 1/this.shellWeight;
		return speed*this.distance;
	}

}
