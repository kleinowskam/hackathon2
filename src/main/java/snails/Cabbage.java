package snails;
import java.util.LinkedList;
import java.util.Queue;

public class Cabbage {

	
    public Queue<GenericSnail> startLeaves = new LinkedList<>();
    private boolean addSnail = false;
    private boolean natureDone = false;

    /**
     * Metoda wrzuca do bloków startowych  auto
     * 
     * @param racer Auto
     * @throws InterruptedException
     */
    public synchronized void put(GenericSnail racer) throws InterruptedException {
        // jeżeli Starter jeszcze nie wystartował to producer czeka
        if(addSnail) {
            wait();
        }

        System.out.println("Dodaję " + racer.getName());
        startLeaves.add(racer); // dodaje do bloku startowego auto
        addSnail = true; // info że dodałem
        notify(); // odblokowanie Startera
    }

    /**
     * Czy kolejka jest pusta
     * 
     * @return Czy kolejka jest pusta
     */
    public boolean isEmpty() {
        return startLeaves.isEmpty();
    }

    /**
     * Czy producer skończył
     * 
     * @return Czy koniec pracy producenta
     */
    public boolean isNatureDone() {
        return natureDone;
    }

    /**
     * Metoda ustawia informację że Producer skończył pracę
     */
    public void natureDone() {
        this.natureDone = true;
    }

    /**
     * Metoda wypuszcza auta z kolejki
     * 
     * @return Wypuszczone auto
     * @throws InterruptedException
     */
    public synchronized GenericSnail start() throws InterruptedException {
        // jeśli Producer jeszcze dodaje to czekam
        if(!addSnail) {
            wait();
        }
        //System.out.println("test " + startLeaves.isEmpty());
        GenericSnail racer = startLeaves.poll(); // pobranie auta
        System.out.println("Wypuszczam " + racer.getName());

        if(racer != null) {
            new Thread(racer).start();
        }
        addSnail = false; // ustawiam info dla Producera sam się blokuję
        notify(); // sciagam wait z Producera

        return racer;
    }
}
