package snails;

import java.util.Random;

public class Nature extends Thread{

	private Cabbage cabbage;
	
	public Nature(Cabbage cabbage){
		this.cabbage = cabbage;
		this.start();
	}
	
	public void run() {
        try {
            Random random = new Random(10);
            // petla produkuje 10 slimakow
            for(int no = 0; no < 10; no++) {
                GenericSnail snail;
                double w = random.nextInt() % 2;
                if(w == 0) {
                    snail = new WinniczekProper();
                } else {
                    snail = new WinniczekNRD();
                }
                String snailName = snail.getClass().getName();
                snail.setName(snailName.substring(snailName.lastIndexOf(".")) + " " + no);
                cabbage.put(snail);
            }
            cabbage.natureDone();
        } catch(InterruptedException e) {
            e.printStackTrace();
        }
        cabbage.natureDone();
    }
}
