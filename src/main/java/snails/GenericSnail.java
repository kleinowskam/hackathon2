package snails;

public abstract class GenericSnail implements Runnable {

	protected int distance;
	private double remDistance;
	private String name;
	protected double shellWeight;
	
	public GenericSnail(){
		distance = 1000;
	}
	
	
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}

	public abstract double crawls();

	public void run() {

	     remDistance = distance; 
	        while(true) {
	            try {
	                Thread.sleep(1000); 
	                double crawlDistance = crawls();
	                System.out.println(crawlDistance);
	                remDistance -= crawlDistance; 
	                System.out.println(name + ": Pozostały dystans to " + remDistance + " m");
	                
	                if(remDistance <= 0) {
	                    System.out.println(name + ": Meta!");
	                    break;
	                }
	            } catch (InterruptedException e) {
	                e.printStackTrace();
	            }
	        }
	}

	
}
