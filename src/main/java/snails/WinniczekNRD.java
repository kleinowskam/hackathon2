package snails;

public final class WinniczekNRD extends GenericSnail {

	public WinniczekNRD(){
		shellWeight = 57;
	}
	@Override
	public double crawls() {
		return ((1/this.shellWeight)*this.distance);
	}

}
