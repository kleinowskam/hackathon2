package snails;

public class SnailReleaser extends Thread {

	private Cabbage cabbage;
	
	public SnailReleaser(Cabbage cabbage){
		this.cabbage = cabbage;
		this.start();
	}
	
	  public void run() {
	        try {
	            while(true) {
	                if(cabbage.isEmpty() && cabbage.isNatureDone()) {
	                    break;
	                }
	                cabbage.start(); 
	            }
	        } catch(InterruptedException e) {
	            e.printStackTrace();
	        }
	    }
}
