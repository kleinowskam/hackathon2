package uInterface;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import mailer.SendMailTLS;

public class MailerApp extends Application{

	public static void main(String[] args) {
        launch(args);
    }
        
    final Button button = new Button ("Send");
    final Label notification = new Label ();
    final TextField subject = new TextField("");
    final TextArea text = new TextArea ("");
    final TextField emailAddress = new TextField("pawel.apanasewicz@codeme.pl");
    
    String address = " ";
    
    @Override public void start(Stage stage) {
        stage.setTitle("HackaMailer");
        Scene scene = new Scene(new Group(), 650, 450);
        emailAddress.setPrefWidth(400);
        
        
        button.setOnMouseClicked(new EventHandler<MouseEvent>(){

			public void handle(MouseEvent event) {
				try {
					notification.setText("");
					SendMailTLS.sendMail(subject.getText(), text.getText(), emailAddress.getText());
					notification.setText("Message sent!");
				} catch (AddressException e) {
					notification.setText("Email error, dude");
					e.printStackTrace();
					
				} catch (MessagingException e) {
					notification.setText("Email error, dude");
					e.printStackTrace();
				}
			}
		});
       
        
        GridPane grid = new GridPane();
        grid.setVgap(4);
        grid.setHgap(10);
        grid.setPadding(new Insets(5, 5, 5, 5));
        grid.add(new Label("To: "), 0, 0);
        grid.add(emailAddress, 1, 0);
        grid.add(new Label("Subject: "), 0, 1);
        grid.add(subject, 1, 1, 3, 1);            
        grid.add(text, 0, 2, 4, 1);
        grid.add(button, 0, 3);
        grid.add (notification, 1, 3, 3, 1);
        
        Group root = (Group)scene.getRoot();
        root.getChildren().add(grid);
        stage.setScene(scene);
        stage.show();
    }    

}
